// Tạo mảng lưu trữ SV
var dssv = [];

// lấy dữ liệu từ localStorage lúc reload

var dataJson = localStorage.getItem("DSSV");
if(dataJson != null) {
    dssv = JSON.parse(dataJson).map(function(item){
        return new SinhVien(item.ma, item.ten, item.email, item.matKhau, item.toan, item.ly, item.hoa);

    });
    renderDSSV(dssv);
};

//Thêm Sinh Viên
function themSinhVien() {
 //Lấy thông tin từ form
    var sv = layThongTinTuForm();
    // kiểm tra MSSV 5 kí tự
    var isValid = 
        kiemTraTrung(sv.ma, dssv) && 
        kiemTraDoDai(5, 5, "spanMaSV", "Mã sinh viên gồm 5 kí tự", sv.ma);
    
    // kiểm tra mật khẩu
    isValid = 
        isValid & 
        kiemTraDoDai(4, 8, "spanMatKhau", "Mật khẩu phải từ 4 đến 8 kí tự", sv.matKhau);
    // kiểm tra Email 
    isValid &= kiemTraEmail(sv.email);
    
    if(isValid) {

       dssv.push(sv);

       renderDSSV(dssv); // render DSSV


       var dataJson = JSON.stringify(dssv);
       localStorage.setItem("DSSV", dataJson); // lưu DSSV vào localStorage

       // reset form in put
       document.getElementById("formQLSV").reset();
    };

}

// Xóa sinh viên 

function xoaSinhVien(id){
    var index = dssv.findIndex(function(item){
        return item.ma == id;
    });
    dssv.splice(index, 1);
    
    renderDSSV(dssv);

    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson); // cập nhật DSSV   mới vào localStorage khi xóa
};

// sửa sinh viên

function suaSinhVien(id){
    var index = dssv.findIndex(function(item){
        return item.ma == id;
    });
    showThongTinLenFrom(dssv[index]);
};

// Cập nhật sinh viên
function capNhatSinhVien() {
    // Lấy thông tin từ form
    var sv = layThongTinTuForm();
    
    // Tìm vị trí của sinh viên trong danh sách dssv
    var index = dssv.findIndex(function(item) {
        return item.ma == sv.ma;
    });

    // Cập nhật thông tin sinh viên khi được sửa
    dssv[index].ten = sv.ten;
    dssv[index].email = sv.email;
    dssv[index].matKhau = sv.matKhau;
    dssv[index].toan = sv.toan;
    dssv[index].ly = sv.ly;
    dssv[index].hoa = sv.hoa;
    
    renderDSSV(dssv); // Render lại danh sách sinh viên
    
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson); // Cập nhật DSSV mới vào localStorage
    
    // Reset form input
    document.getElementById("formQLSV").reset();
}

