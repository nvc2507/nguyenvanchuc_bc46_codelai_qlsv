// dom show message dùng chung

function showMessage(idSpan, message){
    document.getElementById(idSpan).innerText = message;
}

// kiểm tra trùng MSSV
function kiemTraTrung(idSv, dssv) {
    var index = dssv.findIndex(function(item){
        return item.ma == idSv;
    });
    
    if (index == -1) {
        showMessage("spanMaSV", "");
        return true;
    }else {
        showMessage("spanMaSV", "Mã sinh viên đã tồn tại");
        return false;
    };
};

// kiểm tra độ dài kí tự

function kiemTraDoDai(min, max, idSpan, message, value){
    var length = value.length;
    if(length >= min && length <= max){
        showMessage(idSpan, "");
        return true;
    }else{
        showMessage(idSpan, message);
        return false;
    };
};

//Kiểm tra Email

function kiemTraEmail(email){
    const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var  isEmail = re.test(email);
    if(isEmail){
        showMessage("spanEmailSV", "");
        return true;
    }else{
        showMessage("spanEmailSV"," Email không hợp lệ");
        return false;
    }
}